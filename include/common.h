#pragma once

#include <netinet/tcp.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <chrono>
#include <fcntl.h>
#include <signal.h>
#include <string.h>

#include <stdint.h>

#define PING_MESSAGE "PingPongPingPongPingPongPingPongPingPongPingPongPingPongPingPongPingPongPingPongPingPongPingPongPingPongPingPongPingPongPingPong\n"
// #define PING_MESSAGE "Ping\n"
#define BUF_SIZE (int) 1024
#define PING_DELAY 10

inline bool interrupted = false;

struct ThreadParams {
    volatile int64_t ping = 0;
    volatile bool doExit = false;
    volatile bool clientConnected = false;
};

inline int strpos(const char *haystack, const char *needle) {
    const char *p = strstr(haystack, needle);
    if (p){
        return p - haystack;
    }
    return -1;
}

inline void sigHandler([[maybe_unused]] int signum) {
    interrupted = true;
    fflush(stdout);
}
