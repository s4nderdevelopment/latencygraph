#pragma once
#include "common.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

int initializeGlfw();

void terminateGlfw();

GLFWwindow* createWindow(int width, int height);
void destroyWindow(GLFWwindow* window);

void updateWindowEvents();

void runGuiThread(ThreadParams* params, GLFWwindow* glfwWindow);

void useWindow(GLFWwindow* window);
