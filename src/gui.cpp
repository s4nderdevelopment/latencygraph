#ifdef GUI_ENABLED

#include <gui.h>

#define PRECISION (int64_t) 128
#define GRIDLINE_COUNT (int64_t) 4

const std::string vertexShaderCode = R"GLSL(
#version 330

layout(location = 0) in vec2 in_position;

out vec2 v_position;

uniform float u_pingMin;
uniform float u_pingMax;

void main(){
    vec2 position = vec2(in_position.x, (in_position.y - u_pingMin) / (u_pingMax - u_pingMin) * 2.0 - 1.0);
    gl_Position = vec4(position.xy, 0, 1.0);

    v_position = position;
}
)GLSL";

const std::string fragmentShaderCode = R"GLSL(
#version 330

in vec2 v_position;

out vec4 f_color;

void main(){
    float percentagePingMinMax = v_position.y / 2.0 + 0.5;
    f_color = vec4(1.0, 1.0 - (percentagePingMinMax / 2.0), 1.0 - percentagePingMinMax, 1.0);
}
)GLSL";

void runGuiThread(ThreadParams* params, GLFWwindow* window){
    useWindow(window);

    if(!gladLoadGL()){
        fprintf(stderr, "Failed to initialize glad for glfw window\n");
        
        useWindow(nullptr);
        return;
    }

    const char* vShaderCode = vertexShaderCode.c_str();
    const char* fShaderCode = fragmentShaderCode.c_str();

    GLuint vShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vShader, 1, &vShaderCode, nullptr);
    glCompileShader(vShader);

    int status = 0;
    glGetShaderiv(vShader, GL_COMPILE_STATUS, &status);
    if(!status){
        char info[512];
        glGetShaderInfoLog(vShader, 512, nullptr, info);

        fprintf(stderr, "Failed to compile vertex shader: %s\n", info);
        glDeleteShader(vShader);
        return;
    }

    GLuint fShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fShader, 1, &fShaderCode, nullptr);
    glCompileShader(fShader);

    glGetShaderiv(fShader, GL_COMPILE_STATUS, &status);
    if(!status){
        char info[512];
        glGetShaderInfoLog(fShader, 512, nullptr, info);

        fprintf(stderr, "Failed to compile fragment shader: %s\n", info);
        glDeleteShader(vShader);
        glDeleteShader(fShader);
        return;
    }

    GLuint program = glCreateProgram();
    glAttachShader(program, vShader);
    glAttachShader(program, fShader);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &status);
    
    glDetachShader(program, vShader);
    glDetachShader(program, fShader);
    glDeleteShader(vShader);
    glDeleteShader(fShader);
    
    if(!status){
        char info[512];
        glGetProgramInfoLog(program, 512, nullptr, info);

        fprintf(stderr, "Failed to link shader program: %s\n", info);
        glDeleteProgram(program);
        return;
    }

    glm::vec2 lines[PRECISION] = {
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },

        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },

        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },

        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },

        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },

        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },

        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        { 0, 0 }
    };

    glm::vec2 gridLines[GRIDLINE_COUNT * 2] = {
        { -1, 0 },
        { 1, 0 },
        { -1, 0 },
        { 1, 0 },
        { -1, 0 },
        { 1, 0 }
    };

    for(int i = 0; i < PRECISION; i++){
        float x = ((float) i / ((float) (PRECISION - 1)) - 0.5f) * 2.0f;
        lines[i].x = x;
        lines[i].y = -1.0f;
    }

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    glBufferData(GL_ARRAY_BUFFER, (PRECISION + GRIDLINE_COUNT * 2) * sizeof(glm::vec2), nullptr, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
    glEnableVertexAttribArray(0);

    float pingMin = -1;
    float pingMax = 400;
    
    float pingPreferredBelow = (pingMax - pingMin) / GRIDLINE_COUNT + pingMin;

    for(int64_t i = 0; i < GRIDLINE_COUNT; i++){
        gridLines[i * 2].y = pingPreferredBelow * (float) (i + 1LL);
        gridLines[i * 2 + 1].y = pingPreferredBelow * (float) (i + 1LL);
    }

    int u_pingMax = glGetUniformLocation(program, "u_pingMax");
    int u_pingMin = glGetUniformLocation(program, "u_pingMin");

    int fbWidth = 0;
    int fbHeight = 0;

    while (!glfwWindowShouldClose(window) && !params->doExit && params->clientConnected)
    {
        glfwGetFramebufferSize(window, &fbWidth, &fbHeight);

        glViewport(0, 0, fbWidth, fbHeight);

        glUseProgram(program);
        glUniform1f(u_pingMax, pingMax);
        glUniform1f(u_pingMin, pingMin);

        for(int i = 0; i < PRECISION - 1; i++){
            lines[i].y = lines[i + 1].y;
        }
        lines[PRECISION - 1].y = (float) params->ping;

        glBufferSubData(GL_ARRAY_BUFFER, 0, PRECISION * sizeof(glm::vec2), lines);
        glBufferSubData(GL_ARRAY_BUFFER, PRECISION * sizeof(glm::vec2), GRIDLINE_COUNT * 2 * sizeof(glm::vec2), gridLines);
        
        glClearColor(0.5, 0.5, 0.5, 1.0);
        glClear(GL_COLOR_BUFFER_BIT);
        glDrawArrays(GL_LINE_STRIP, 0, PRECISION);
        glDrawArrays(GL_LINES, PRECISION, GRIDLINE_COUNT * 2);

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        fprintf(stdout, "Latency (TCP): %lldms\n", params->ping);

        usleep(1000 * 100);
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDeleteBuffers(1, &vbo);

    glBindVertexArray(0);
    glDeleteVertexArrays(1, &vao);

    interrupted = true;
}
#endif
