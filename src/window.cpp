#ifdef GUI_ENABLED

#include <gui.h>

void errorFunc(int code, const char* description){
    fprintf(stderr, "GLFW error %d: %s\n", code, description);
}

int initializeGlfw(){
    glfwSetErrorCallback(errorFunc);
    
    return glfwInit();
}

void terminateGlfw() {
    glfwTerminate();
}

GLFWwindow* createWindow(int width, int height) {
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    /* Create a windowed mode window and its OpenGL context */
    GLFWwindow* window = glfwCreateWindow(width, height, "LatencyGraph - Client", NULL, NULL);
    if (!window)
    {
        return NULL;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    glfwSwapInterval(0);
    // glfwSwapInterval(1);

    glfwMakeContextCurrent(NULL);

    return window;
}

void destroyWindow(GLFWwindow* window) {
    glfwDestroyWindow(window);
}

void useWindow(GLFWwindow* window){
    glfwMakeContextCurrent(window);
}

void updateWindowEvents(){
    glfwPollEvents();
}

#endif
