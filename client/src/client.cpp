#include <common.h>
#ifdef GUI_ENABLED
    #include <gui.h>
#endif

#include <thread>
#include <string>

int main(int argc, const char** argv){
    signal(SIGPIPE, sigHandler);
    if(signal(SIGINT, sigHandler) == SIG_ERR){
        fprintf(stderr, "SIGINT handler failed to register\n");
        return -1;
    }

    if(argc < 2){
        fprintf(stderr, "Please use: %s <address> [port] [message|len:<message_length>] [mtu]\n", argv[0]);
        return -1;
    }

    FILE* f = fopen("log.txt", "a");
    
    FILE* fp = popen("/bin/date", "r");
    if (fp == NULL) {
        printf("Failed to run command\n" );
        exit(1);
    }

    char date[1024];

    /* Read the output a line at a time - output it. */
    while (fgets(date, sizeof(date), fp) != NULL) {
        fprintf(f, "%s", date);
    }
    fprintf(f, "\n");

    fclose(fp);

    int port;
    int mtu = 0;
    std::string messageStr = PING_MESSAGE;

    if(argc >= 3){
        port = atoi(argv[2]);
        if(argc >= 4){
            messageStr = argv[3];
            size_t len = messageStr.length();
            std::string compareWith = "len:";
            size_t compareLen = compareWith.length();
            if(len >= compareLen){
                if(messageStr.substr(0, compareLen) == compareWith){
                    std::string lengthStr = messageStr.substr(compareLen);
                    int length = atoi(lengthStr.c_str());
                    if(length >= 1){
                        messageStr = "";
                        for(int i = 0; i < length; i++){
                            messageStr += "A";
                        }
                    }
                }
            }
            messageStr += "\n";
            fprintf(stdout, "Using message: %s\n", messageStr.c_str());
            if(argc >= 5){
                mtu = atoi(argv[4]);
                fprintf(stdout, "Using mtu: %d\n", mtu);
            }
        }
    }else{
        port = 4000;
    }

    const char* message = messageStr.c_str();

    int clientSocket = socket(AF_INET, SOCK_STREAM, 0);

    if(clientSocket == -1){
        fprintf(stderr, "Could not create socket.\n");
        return -1;
    }

    int size = sizeof(sockaddr_in);

    sockaddr_in server;
    server.sin_addr.s_addr = inet_addr(argv[1]);
    server.sin_family = AF_INET;
    server.sin_port = htons((uint16_t) port);

    if(connect(clientSocket, (struct sockaddr*) &server, size) < 0){
        fprintf(stderr, "Could not connect to %s on port %d.\n", argv[1], port);
        return -1;
    }

    int flag = 1;
    setsockopt(clientSocket, SOL_TCP, TCP_NODELAY, &flag, sizeof(int));
    
    struct timeval tv;
    tv.tv_sec = 5;
    tv.tv_usec = 0;
    setsockopt(clientSocket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));
    if(mtu > 0){
        setsockopt(clientSocket, IPPROTO_TCP, TCP_MAXSEG, &mtu, sizeof(mtu));
    }

    char rdbuf[BUF_SIZE];
    size_t sndbufLen = strlen(message) + 1;

    fprintf(stdout, "Connected.\n");

    memset(rdbuf, 0, BUF_SIZE);

    ThreadParams params;

    params.clientConnected = true;

#ifdef GUI_ENABLED
    GLFWwindow* window = nullptr;
    std::thread th;
    
    if(!initializeGlfw()){
        fprintf(stderr, "Failed to initialize glfw.\n");
    }else{
        window = createWindow(640, 480);
        if(window == nullptr){
            fprintf(stderr, "Failed to create glfw window.\n");
        }else{
            th = std::thread(runGuiThread, &params, window);
        }
    }
#endif

    std::string readResult;

    usleep(1000 * 1000);

    int iterations = 0;

    while(!params.doExit && params.clientConnected){
        ssize_t readCount = 0;

        int pos = strpos(readResult.c_str(), "\n");

        while(pos == -1){
            readCount = recv(clientSocket, rdbuf, BUF_SIZE - 1, 0/*MSG_DONTWAIT*/);
            
            int error = errno;
            if(readCount <= 0 || interrupted){
                params.clientConnected = false;
                break;
            }
            
            rdbuf[readCount] = '\0';

            readResult += rdbuf;
            
            pos = strpos(readResult.c_str(), "\n");
        }

        readResult = readResult.substr(pos + 1);

        auto startTime = std::chrono::steady_clock::now();
        
        for(size_t i = 0; i < sndbufLen; i += BUF_SIZE){
            size_t size = BUF_SIZE;
            if(sndbufLen - i < BUF_SIZE){
                size = sndbufLen - i;
            }
            ssize_t sendCount = send(clientSocket, message + i, size - 1, 0/*MSG_DONTWAIT*/);

            if(sendCount <= 0 && errno != EWOULDBLOCK){
                params.clientConnected = false;
            }
        }
        setsockopt(clientSocket, SOL_TCP, TCP_NODELAY, &flag, sizeof(int));

        pos = strpos(readResult.c_str(), "\n");

        while(pos == -1){
            readCount = recv(clientSocket, rdbuf, BUF_SIZE - 1, 0/*MSG_DONTWAIT*/);
            
            if(readCount <= 0 || interrupted){
                params.clientConnected = false;
                break;
            }
            
            rdbuf[readCount] = '\0';

            readResult += rdbuf;

            pos = strpos(readResult.c_str(), "\n");
        }

        auto diff = std::chrono::steady_clock::now() - startTime;
        
        for(size_t i = 0; i < sndbufLen; i += BUF_SIZE){
            size_t size = BUF_SIZE;
            if(sndbufLen - i < BUF_SIZE){
                size = sndbufLen - i;
            }
            ssize_t sendCount = send(clientSocket, message + i, size - 1, 0/*MSG_DONTWAIT*/);

            if(sendCount <= 0 && errno != EWOULDBLOCK){
                params.clientConnected = false;
            }
        }
        setsockopt(clientSocket, SOL_TCP, TCP_NODELAY, &flag, sizeof(int));

        std::string toPrint = readResult.substr(0, pos);
        readResult = readResult.substr(pos + 1);

        int64_t micros = std::chrono::duration_cast<std::chrono::microseconds>(diff).count();

        // fprintf(stdout, "Message: %s\n", toPrint.c_str());
        // fprintf(stdout, "Latency (TCP): %lld.%003lldms\n", micros / 1000LL, micros % 1000LL);
        params.ping = micros / 1000LL;
        fprintf(f, "%lldms\n", micros / 1000LL);

        memset(rdbuf, 0, BUF_SIZE);
        usleep(1000 * PING_DELAY);

#ifdef GUI_ENABLED
        if(window != nullptr){
            updateWindowEvents();
        }
#endif

        iterations++;
    }

    shutdown(clientSocket, SHUT_RDWR);
    close(clientSocket);

    params.clientConnected = false;

#ifdef GUI_ENABLED
    if(window != nullptr){
        th.join();
        destroyWindow(window);
    }

    terminateGlfw();
#endif
    
    fprintf(stdout, "Done. %d iterations.\n", iterations);
    fclose(f);

    return 0;
}
