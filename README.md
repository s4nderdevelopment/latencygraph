# LatencyGraph

Measure latency using the TCP protocol to troubleshoot issues for gaming (ping spikes or high ping).

## Prerequisites

Windows: Coming soon, currently this tool only works on Linux systems

Linux:
- Dependencies `apt`/`apt-get`:
  - `make`, `build-essential`, `g++` and `cmake`
  - `xorg-dev` (for GLFW)
  - `libgl1-mesa-dev` and `libglu1-mesa-dev` (for GLAD, these packages are probably preinstalled already if you have a desktop environment)

## How to compile

Linux:
```bash
git clone https://gitlab.com/s4nderdevelopment/latencygraph
cd latencygraph
chmod +x ./clone-dependencies.sh
./clone-dependencies.sh
# For building the client and server application with GUI (requires more dependencies)
./build.sh
# For building the client and server application without GUI
./build_nogui.sh
```

## How to use (server)

Linux:
```bash
./build/LatencyGraphServer <listen-interface> [port] [message|len:<message_length>] [mtu]
# For example:
./build/LatencyGraphServer 0.0.0.0
# Or:
./build/LatencyGraphServer 0.0.0.0 4001
# Or:
./build/LatencyGraphServer 0.0.0.0 4001 abcd
# Or:
./build/LatencyGraphServer 0.0.0.0 4001 len:1024
```

## How to use (client with GUI)

Linux:
```bash
./build/LatencyGraphClient <ip-address> [port] [message|len:<message_length>] [mtu]
# For example:
./build/LatencyGraphClient 127.0.0.1
# Or:
./build/LatencyGraphClient 127.0.0.1 4001
# Or:
./build/LatencyGraphClient 127.0.0.1 4001 abcd
# Or:
./build/LatencyGraphClient 127.0.0.1 4001 len:1024
```

## How to use (client no GUI)

Linux:
```bash
./build/LatencyGraphClientNoGUI <ip-address> [port] [message|len:<message_length>] [mtu]
# For example:
./build/LatencyGraphClient 127.0.0.1
# Or:
./build/LatencyGraphClient 127.0.0.1 4001
# Or:
./build/LatencyGraphClient 127.0.0.1 4001 abcd
# Or:
./build/LatencyGraphClient 127.0.0.1 4001 len:1024
```

## TODO

- [ ] Hostname resolve support
- [ ] GUI support for server
- [ ] Multiple connections at the same time for server
- [ ] Better command line arguments parsing with support for options
- [ ] GUI improvements
  - [ ] Shader for changing line color
  - [ ] Window resize support
  - [ ] Options for the graph range
