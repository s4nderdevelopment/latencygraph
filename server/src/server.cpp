#include <common.h>
#include <thread>
#include <string>
#include <string.h>

int main(int argc, const char** argv){
    signal(SIGPIPE, sigHandler);

    if(argc < 2){
        fprintf(stderr, "Please use: %s <listen-interface> [port] [message|len:<message_length>] [mtu]\n", argv[0]);
        return -1;
    }

    int port;
    int mtu = 0;

    std::string messageStr = PING_MESSAGE;

    if(argc >= 3){
        port = atoi(argv[2]);
        if(argc >= 4){
            messageStr = argv[3];
            size_t len = messageStr.length();
            std::string compareWith = "len:";
            size_t compareLen = compareWith.length();
            if(len >= compareLen){
                if(messageStr.substr(0, compareLen) == compareWith){
                    std::string lengthStr = messageStr.substr(compareLen);
                    int length = atoi(lengthStr.c_str());
                    if(length >= 1){
                        messageStr = "";
                        for(int i = 0; i < length; i++){
                            messageStr += "A";
                        }
                    }
                }
            }
            messageStr += "\n";
            fprintf(stdout, "Using message: %s\n", messageStr.c_str());
            if(argc >= 5){
                mtu = atoi(argv[4]);
                fprintf(stdout, "Using mtu: %d\n", mtu);
            }
        }
    }else{
        port = 4000;
    }
    const char* message = messageStr.c_str();

    int serverSocket = socket(AF_INET, SOCK_STREAM, 0);

    if(serverSocket == -1){
        fprintf(stderr, "Could not create socket.\n");
        return -1;
    }

    int val = 1;
    setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));

    int size = sizeof(sockaddr_in);

    sockaddr_in server;
    server.sin_addr.s_addr = inet_addr(argv[1]);
    server.sin_family = AF_INET;
    server.sin_port = htons((uint16_t) port);

    if(bind(serverSocket, (struct sockaddr*) &server, size) < 0){
        fprintf(stderr, "Could not bind socket on port %d.\n", port);
        return -1;
    }

    if(listen(serverSocket, SOMAXCONN) < 0){
        fprintf(stderr, "Could not listen on port %d.\n", port);
        return -1;
    }

    sockaddr_in client;
    int clientSocket;
    char rdbuf[BUF_SIZE];
    size_t sndbufLen = strlen(message) + 1;

    memset(rdbuf, 0, BUF_SIZE);
    // memset(sndbuf, 0, BUF_SIZE);
    // memcpy(sndbuf, message, strlen(message) + 1);

    fprintf(stdout, "Server is ready to accept connections.\n");

    ThreadParams params;

    while(!params.doExit){
        clientSocket = accept(serverSocket, (struct sockaddr*) &client, (socklen_t*)&size);

        if(clientSocket < 0){
            break;
        }
        
        if(signal(SIGINT, sigHandler) == SIG_ERR){
            fprintf(stderr, "SIGINT handler failed to register\n");
            return -1;
        }

        int flag = 1;
        setsockopt(clientSocket, SOL_TCP, TCP_NODELAY, &flag, sizeof(int));
        if(mtu > 0){
            setsockopt(clientSocket, IPPROTO_TCP, TCP_MAXSEG, &mtu, sizeof(mtu));
        }
        
        struct timeval tv;
        tv.tv_sec = 5;
        tv.tv_usec = 0;
        setsockopt(clientSocket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));

        params.clientConnected = true;

        fprintf(stdout, "Accepted connection.\n");

        std::string readResult;

        usleep(1000 * 1000);

        int iterations = 0;

        while(!params.doExit && params.clientConnected){
            auto startTime = std::chrono::steady_clock::now();
            // ssize_t sendCount = send(clientSocket, sndbuf, BUF_SIZE - 1, 0/*MSG_DONTWAIT*/);
            // setsockopt(clientSocket, SOL_TCP, TCP_NODELAY, &flag, sizeof(int));

            // if(sendCount <= 0 && errno != EWOULDBLOCK){
            //     params.clientConnected = false;
            // }
            for(size_t i = 0; i < sndbufLen; i += BUF_SIZE){
                size_t size = BUF_SIZE;
                if(sndbufLen - i < BUF_SIZE){
                    size = sndbufLen - i;
                }
                ssize_t sendCount = send(clientSocket, message + i, size - 1, 0/*MSG_DONTWAIT*/);

                if(sendCount <= 0 && errno != EWOULDBLOCK){
                    params.clientConnected = false;
                }
            }
            setsockopt(clientSocket, SOL_TCP, TCP_NODELAY, &flag, sizeof(int));

            ssize_t readCount = 0;

            int pos = strpos(readResult.c_str(), "\n");

            while(pos == -1){
                readCount = recv(clientSocket, rdbuf, BUF_SIZE - 1, 0/*MSG_DONTWAIT*/);
                
                if(readCount <= 0 || interrupted){
                    params.clientConnected = false;
                    break;
                }
                rdbuf[readCount] = '\0';
                readResult += rdbuf;
                pos = strpos(readResult.c_str(), "\n");
            }

            auto diff = std::chrono::steady_clock::now() - startTime;
            
            std::string toPrint = readResult.substr(0, pos);
            readResult = readResult.substr(pos + 1);

            for(size_t i = 0; i < sndbufLen; i += BUF_SIZE){
                size_t size = BUF_SIZE;
                if(sndbufLen - i < BUF_SIZE){
                    size = sndbufLen - i;
                }
                ssize_t sendCount = send(clientSocket, message + i, size - 1, 0/*MSG_DONTWAIT*/);

                if(sendCount <= 0 && errno != EWOULDBLOCK){
                    params.clientConnected = false;
                }
            }
            setsockopt(clientSocket, SOL_TCP, TCP_NODELAY, &flag, sizeof(int));

            readCount = 0;

            pos = strpos(readResult.c_str(), "\n");

            while(pos == -1){
                readCount = recv(clientSocket, rdbuf, BUF_SIZE - 1, 0/*MSG_DONTWAIT*/);
                
                if(readCount <= 0 || interrupted){
                    params.clientConnected = false;
                    break;
                }
                rdbuf[readCount] = '\0';
                readResult += rdbuf;
                pos = strpos(readResult.c_str(), "\n");
            }
            readResult = readResult.substr(pos + 1);
            
            int64_t micros = std::chrono::duration_cast<std::chrono::microseconds>(diff).count();

            // fprintf(stdout, "Message: %s\n", toPrint.c_str());
            // fprintf(stdout, "Latency (TCP): %lld.%003lldms\n", micros / 1000LL, micros % 1000LL);
            params.ping = micros / 1000LL;

            memset(rdbuf, 0, BUF_SIZE);
            usleep(1000 * PING_DELAY);

            iterations++;
        }

        shutdown(clientSocket, SHUT_RDWR);
        close(clientSocket);

        params.clientConnected = false;

        if(signal(SIGINT, SIG_DFL) == SIG_ERR){
            fprintf(stderr, "SIGINT handler failed to register\n");
            return -1;
        }
        interrupted = false;

        fprintf(stdout, "Closed connection. %d iterations.\n", iterations);
    }

    close(serverSocket);

    fprintf(stdout, "Done\n");

    return 0;
}
