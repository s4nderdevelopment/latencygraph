if [ -d "dependencies" ]; then
    read -p "Dependencies already cloned. Reclone? [y/N]: " reclone
    reclone=${reclone:-n}
    if [ $reclone != "y" -a $reclone != "Y" ]; then
        echo "Aborted"
        exit
    fi

    rm -Rf dependencies
fi

mkdir dependencies

echo "Cloning dependencies..."
echo "Cloning GLFW 3.3.6 and checking out commit hash 7d5a16ce714f0b5f4efa3262de22e4d948851525"
git clone -n https://github.com/glfw/glfw dependencies/glfw
cd dependencies/glfw && git checkout 7d5a16ce714f0b5f4efa3262de22e4d948851525; cd ../..

echo "Cloning GLAD and checking out commit hash 1ecd45775d96f35170458e6b148eb0708967e402"
git clone -n https://github.com/Dav1dde/glad dependencies/glad
cd dependencies/glad && git checkout 1ecd45775d96f35170458e6b148eb0708967e402; cd ../..

echo "Cloning GLM and checking out commit hash 6ad79aae3eb5bf809c30bf1168171e9e55857e45"
git clone -n https://github.com/g-truc/glm dependencies/glm
cd dependencies/glm && git checkout 6ad79aae3eb5bf809c30bf1168171e9e55857e45; cd ../..
